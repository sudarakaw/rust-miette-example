# Example project to try out `Clap v4` & `miette`.

Example project based on following videos by [chris biscardi](https://hachyderm.io/@chrisbiscardi).

- [Generating colors with Clap v4 and palette](https://www.youtube.com/watch?v=ThxvuMman28)
- [Fancy error reporting with miette](https://www.youtube.com/watch?v=NEz_gCNwt-Q)

## License

Copyright 2023 Sudaraka Wijesinghe <sudaraka@sudaraka.org>

This program comes with ABSOLUTELY NO WARRANTY;
This is free software, and you are welcome to redistribute it and/or modify it
under the terms of the BSD 2-clause License. See the LICENSE file for more
details.
